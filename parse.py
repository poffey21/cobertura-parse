import sys
import re

def main():
    """
    parse
    :return:
    """
    if len(sys.argv) != 2:
        print('Only willing to parse a single file')
        exit(1)

    with open(sys.argv[1]) as f:
        data = f.read()

    m = re.findall(r'coverage line-rate="(\d+.\d+)"', data)
    print(f'coverage line-rate="{float(m[-1]) * 100:.2f}"')


if __name__ == '__main__':
    main()
